/**
 * Created by Blake on 4/27/2017.
 */
"use strict";
var vendorId = 'd4861c6b-5393-4795-bef0-ae7918e8d8f0';
var appId = 'd69c531a-58eb-4676-a43c-d8b53015defa';
var baseURL = "https://r2w3json.running2win.com/r2w3API.asmx";
var running2win = angular.module('running2win', ['ngRoute']);

running2win.config(function($routeProvider, $httpProvider){
    $routeProvider
        .when('/', {
            templateUrl: 'Login.html',
            controller: 'loginController'
        })

        .when('/routes', {
        templateUrl: 'Routes.html',
        controller: 'routesController'
        })

        .when('/custom_routes', {
        templateUrl: 'Custom_Route.html',
        controller: 'custom_routesController'
        })

        .when('/calendar/:month/:year', {
        templateUrl: 'Running_Log.html',
        controller: 'calendarController'
        })

        .when('/workout/:month/:day/:year', {
            templateUrl: 'Daily_Workouts.html',
            controller: 'workoutController'
        })

        .when('/tools', {
        templateUrl: 'Tools.html',
        controller: 'toolsController'
        });

    //sets all POST requests to contain Content-Type header
    $httpProvider.defaults.headers.post = {'Content-Type': 'application/x-www-form-urlencoded'};
});

running2win.controller('loginController',function($scope, $rootScope, $http, $httpParamSerializerJQLike,$location){
    $rootScope.name = 'Login';
    $scope.status = '';
    $scope.loginSubmit= function(username, pass){
        var userData = {'vendorId':vendorId, 'appId':appId, 'userNameOrEmail':username, 'password':pass};
        $http({method:'POST',
            url:baseURL+"/login",
            data:$httpParamSerializerJQLike(userData)
            //headers:{'Content-Type': 'application/x-www-form-urlencoded'}
        })
            .success(function(response){
                response = response[0];
            if(response.errorMessage){
                 console.log("error");
                     $scope.status = response.errorMessage;
            }else{
                $rootScope.token = response.token;
                $rootScope.guid = response.guid;
                //TODO update to use current month
                $location.path('/calendar/4/2017');
            }
        }).error(
            function(err){
              $scope.status = "Something went wrong. Please try again.";
        });
    };

});

running2win.controller('routesController',function($scope, $rootScope, $http){
    $rootScope.name = 'Routes';
    $scope.init = function(){
        //TODO: add function to get routes for user when added to API
        //$http.post(baseURL+"")
    };
});

running2win.controller('custom_routesController',function($scope, $rootScope){
    $rootScope.name = 'Custom Route';

});

running2win.controller('calendarController',function($scope, $rootScope, $routeParams){
    $rootScope.name = 'Running Log';
    var months = ['January','February','March','April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    $scope.month = months[$routeParams.month-1]+" "+$routeParams.year;
});

running2win.controller('workoutController',function($scope, $rootScope, $http, $routeParams,$httpParamSerializerJQLike){
    var date = $routeParams.month+"/"+$routeParams.day+"/"+$routeParams.year;
    $rootScope.name = date;
    $scope.workouts = [{
        "workoutKey": 16445271,
        "totalDistance": 5,
        "shortDescription": "Mi",
        "totalDistanceInMiles": 5,
        "totalTime": "18:00:00",
        "runTypeDescription": "Normal Run",
        "comments": "",
        "completed": true,
        "timeOfDayDescription": "Afternoon",
        "isRace": false,
        "raceDistance": 0,
        "raceTypeDescription": "Mi",
        "pacePerRaceDistance": "",
        "pacePerDistance": "3:36:00",
        "pacePerMile": "3:36:00",
        "pacePerKm": "2:14:13"
    },
    {
        "workoutKey": 16445276,
        "totalDistance": 1,
        "shortDescription": "Mi",
        "totalDistanceInMiles": 1,
        "totalTime": "17:00",
        "runTypeDescription": "Normal Run",
        "comments": "",
        "completed": true,
        "timeOfDayDescription": "Afternoon",
        "isRace": false,
        "raceDistance": 0,
        "raceTypeDescription": "Mi",
        "pacePerRaceDistance": "",
        "pacePerDistance": "17:00",
        "pacePerMile": "17:00",
        "pacePerKm": "10:34"
    }];
    $scope.init = function(){
        var data = $httpParamSerializerJQLike({'token':$rootScope.token, 'date': date, 'includeIncomplete':'true','accountGUID':$rootScope.guid});
        console.log(data);
        $http.post(baseURL+'/getLazyLoadWorkoutsForDay', data).then(function(response){
            console.log(response);
        },
         function(err){
            console.log(err+"something went wrong"+data);
             
        });
    };
});

running2win.controller('toolsController',function($scope, $rootScope){
    $rootScope.name = 'Tools';
});
running2win.controller('initController',function($scope, $rootScope){
    $rootScope.token = null;
    $rootScope.guid = null;
});
