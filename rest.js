var vendorId= "see email"; 
var appId= "see email";
var token = localStorage("token");
var guid= localStorage("guid");

function getFavorites(token, count){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getFavorites",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token,
            "count": count
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getLatestTalk(token, count){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getLatestTalk",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token,
            "count": count
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getLazyLoadMailMessages(token, folder, pageNum, perPage){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getLazyLoadMailMessages",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
        "token": token,
        "folderKey": folder,
        "pageNum": pageNum,
        "recordsPerPage": perPage
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getLazyLoadWorkoutsForDateRange(token, startDate, endDate, includeIncomplete){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getLazyLoadWorkoutsForDateRange",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token, 
            "startDate": startDate,
            "endDate": endDate,
            "includeIncomplete": includeIncomplete
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getLazyLoadWorkoutsForDay(token, date, includeIncomplete){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getLazyLoadWorkoutsForDay",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token,
            "date": date,
            "includeIncomplete": includeIncomplete
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getMemberDistancePerYear(token, accountGUID){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getMemberDistancePerYear",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token, 
            "AccountGUID": accoutGUID
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getQuickFriendList(token, accountGUID, count){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getQuickFriendList",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token,
            "accountGUID": accountGUID,
            "count": count
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getRandomVideos(token, count){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getRandomVideos",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token,
            "count": count
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function getTeamList(token){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/getTeamList",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function logOutUser(token){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/logOutUser",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function login(email, pass){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/login",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "vendorId": vendorId,
            "appId": appId,
            "userNameOrEmail": email,
            "password": pass
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function quickMemberSearch(token, searchValue, count){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/quickMemberSearch",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "token": token,
            "searchValue": searchValue,
            "count": count
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

function top20(returnMales){
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "https://r2w3json.running2win.com/r2w3API.asmx/top20",
        "method": "POST",
        "headers": {
            "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
            "returnMales": "true"
        }
    }

    $.ajax(settings).done(function (response) {
        console.log(response);
    });
    return null;
}

